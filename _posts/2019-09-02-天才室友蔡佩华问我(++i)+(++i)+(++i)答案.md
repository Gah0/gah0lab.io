 # Instereing answer with gcc and clang compiler

![avatar](https://github.com/Gah0/Gah0.github.io/blob/master/images/caipeihua.png?raw=true)

此头像经蔡佩华同意上传，该图片所有解释权归蔡佩华

闲来无事搞C语言，天才室友“蔡佩华”问我(++I)+(++I)+(++I)在C语言怎么解，结果Vb得出答案是22，我就按照自己思路想，先自增后加法末赋值这样解释，其实我错了。  
于是换了个C语言编译器试了一下，太狗血了！    
蔡佩华说这个等于21（金坷垃，谁说对了就给他？）  
百度一看，Vscode其他编译器还有等于24的，草，这答案都不正确吗？   
于是自己研究一番。。。    

**结果，其实大家都没有错！！！**  

> recently,I'm compiling with android kernel,according to natherchance what post in github, I Choose clang for my kernel compiler.
study with C programmaning, I confuse what different from gcc and clang compiler. one day, CAIPEIHUA genius ask me a question,
how to get (++i)(++i)(++i) answer, but funny, I got a two values between the compiler
look at this!

CODE:

    #include "stdio.h"
    
    int main() {
    	int d=3;
    	printf("%d",(++i)+(++i)+(++i));
    	return 0;;
    }


**I GOT TWO VALUE FOR ANSWER!!**

GCC-7.4.0 COMPILER:

> $: gcc 运算符和表达式.c && ./a.out

> $:16


CLANG-8.1.0 COMPILER:
> $: clang 运算符和表达式.c && ./a.out

> $:15


so, I reversed code for analysis.

#reversed assembly(AT&T format) 反汇编
**的确不是人看的代码**

`$: objdump -d a.out > gcc.dmp`

![avatar](https://github.com/Gah0/Gah0.github.io/blob/master/images/gcc.png?raw=true)

> 可以得出，先自增两次，相加，再自增，再相加，后赋值。
gcc是不是不符合规范？他并不像书上说的，自增比加号的优先级高。
the self-added operator seem not higher priority,it always self-add, and than register and register add.

------------
**我们来看看clang编译器怎么处理**
**Let's see clang compiler**

`$: objdump -d a.out > clang.dmp`

![avatar](https://github.com/Gah0/Gah0.github.io/blob/master/images/clang.png?raw=true)

> 我也得到clang的汇编结果，先自增，后相加，末赋值，严格按照了运算符规范！

> I also get clang assembly, first self-add, then add, end assignment, strictly follow the operator specification!
> The answer is also correct
---------

#结论
经过这次的工作，我了解了编译器工作原理。
还有解决了天才蔡佩华所提问的问题。
我和他的答案都是正确的，只是编译器工作原理不同导致结果不一样
在汇编过程，我们发现，clang编译器经常调用内存堆栈和cpu寄存器，这可能导致开发板或手机性能下降的原因(随着手机硬件带宽发展速度可忽略不计？)
据我了解，苹果手机系统开发和新的安卓系统编译几乎采用clang。这可能是个大趋势。
我以后编译内核仍然采取gcc，gcc唯一缺点就是缺少报错和编译缓慢。
最后，clang编译器赛高，gcc编译器赛高！


finally, I learned how the compiler works.
and also solved this shit problem what genius “CaiPeihua” asked
All answer are correct, but the compiler works differently and the results are different.
In the assembly process, we found that the clang compiler often calls the memory stack and cpu registers, which may cause the performance of the development board(arm) or mobile phone to decline (as the speed of hardware bandwidth is negligible?)
AFAIK, iPhone system development and the new Android system compilation almost use clang for compiler. This may be a big trend.
But I will still use gcc for compiling. The only drawback of gcc is the lack of -werror and slow compilation.
enjoy!!!
