---
layout: page
type: about
---

  	生活越来越美好啦

​    我非常喜欢上网，看书，睡觉，兴趣是编曲和编程。
​    在读电子类的专业，但是我喜欢计算机更多一些。
​    我的数学，外语不行，日语，模电数电PLC等课程差点歇菜。
​    目前在学语言有C/C++, Python, css, html, shell, PLC。

​	另外，我是一个android/Linux内核爱好者，经常在Android/Linux Kernel做backport，
	曾从高内核版本4.18反向移植EAS负载调度器/f2fs(Flash Friendly File System)到
	linux-3.10.y分支给手机，并成功开机使用（仍然在更新）。

​	对开源社区的k8s，docker感兴趣。

​	喜欢汉堡可乐炸鸡翅等...

​	喜欢电子音乐，日系摇滚。

​	同时也是一个鬼畜up主，音乐制作人，鼓手。

​	小心二次元。